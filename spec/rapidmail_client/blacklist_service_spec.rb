# spec/rapidmail_client/blacklist_service_spec.rb

require "spec_helper"

RSpec.describe RapidmailClient::BlacklistService do
  let(:client) { RapidmailClient::Client.new("test_user", "test_password") }
  let(:blacklist_service) { described_class.new(client) }
  let(:base_url) { RapidmailClient::Client::API_BASE_URL }

  before do
    stub_request(:get, %r{#{base_url}/blacklist?.*})
      .with(basic_auth: %w[test_user test_password])
      .to_return(status: 200, body: { "_embedded" => { "blacklist" => [] }, "page" => 1,
                                      "page_count" => 1 }.to_json, headers: { "Content-Type" => "application/json" })

    stub_request(:get, "#{base_url}/blacklist/blacklist_id")
      .with(basic_auth: %w[test_user test_password])
      .to_return(status: 200, body: { "id" => 1, "type" => "email", "pattern" => "test@example.com" }.to_json,
                 headers: { "Content-Type" => "application/json" })

    stub_request(:post, "#{base_url}/blacklist")
      .with(basic_auth: %w[test_user test_password], body: { "pattern" => "test@example.com" }.to_json)
      .to_return(status: 200, body: { "id" => 1, "type" => "email", "pattern" => "test@example.com" }.to_json,
                 headers: { "Content-Type" => "application/json" })

    stub_request(:delete, "#{base_url}/blacklist/email_id")
      .with(basic_auth: %w[test_user test_password])
      .to_return(status: 204)
  end

  describe "#list" do
    it "retrieves the blacklist items" do
      response = blacklist_service.list
      expect(response[:blacklist]).to eq([])
      expect(response[:page]).to eq(1)
      expect(response[:page_count]).to eq(1)
    end
  end

  describe "#list_all" do
    it "retrieves all blacklisted emails" do
      response = blacklist_service.list_all
      expect(response).to eq([])
    end
  end

  describe "#show" do
    it "retrieves a blacklist item" do
      response = blacklist_service.show("blacklist_id")
      expect(response["id"]).to eq(1)
    end
  end

  describe "#add_to_blacklist" do
    it "adds emails to the blacklist" do
      response = blacklist_service.add_to_blacklist("test@example.com")
      expect(response["id"]).to eq(1)
    end
  end

  describe "#remove_from_blacklist" do
    it "removes an email from the blacklist" do
      expect { blacklist_service.remove_from_blacklist("email_id") }.not_to raise_error
    end
  end
end
