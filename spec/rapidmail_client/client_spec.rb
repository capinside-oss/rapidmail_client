# spec/rapidmail_client/client_spec.rb

require "spec_helper"

RSpec.describe RapidmailClient::Client do
  let(:client) { described_class.new("test_user", "test_password") }
  let(:base_url) { RapidmailClient::Client::API_BASE_URL }

  before do
    stub_request(:get, "#{base_url}/test_endpoint")
      .with(basic_auth: %w[test_user test_password])
      .to_return(status: 200, body: { "result" => "success" }.to_json,
                 headers: { "Content-Type" => "application/json" })

    stub_request(:post, "#{base_url}/test_endpoint")
      .with(basic_auth: %w[test_user test_password], body: { "key" => "value" }.to_json)
      .to_return(status: 200, body: { "result" => "success" }.to_json,
                 headers: { "Content-Type" => "application/json" })

    stub_request(:delete, "#{base_url}/test_endpoint")
      .with(basic_auth: %w[test_user test_password])
      .to_return(status: 200, body: { "result" => "success" }.to_json,
                 headers: { "Content-Type" => "application/json" })
  end

  describe "#get" do
    it "makes a GET request and returns the parsed response" do
      response = client.get("test_endpoint")
      expect(response).to eq("result" => "success")
    end
  end

  describe "#post" do
    it "makes a POST request and returns the parsed response" do
      response = client.post("test_endpoint", { "key" => "value" })
      expect(response).to eq("result" => "success")
    end
  end

  describe "#delete" do
    it "makes a DELETE request and returns the parsed response" do
      response = client.delete("test_endpoint")
      expect(response).to eq("result" => "success")
    end
  end
end
