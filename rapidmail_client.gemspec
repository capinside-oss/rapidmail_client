# frozen_string_literal: true

# rapidmail_client.gemspec
require_relative "lib/rapidmail_client/version"

Gem::Specification.new do |spec|
  spec.name          = "rapidmail_client"
  spec.version       = RapidmailClient::VERSION
  spec.authors       = ["Burkhard Vogel-Kreykenbohm"]
  spec.email         = ["bvk@capinside-solutions.com"]

  spec.summary       = "Ruby client for Rapidmail API"
  spec.description   =
    "A Ruby client for interacting with the Rapidmail API, including support for managing blacklists."
  spec.homepage      = "https://gitlab.com/capinside-oss/rapidmail_client"
  spec.license       = "MIT"
  spec.required_ruby_version = ">= 2.6"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:spec/rapidmail.*|spec/spec_helper)|\A(?:\.git)})
    end
  end
  spec.bindir = "bin"
  spec.executables = spec.files.grep(%r{\Abin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "json", "~> 2.0"
  spec.add_dependency "net-http", "~> 0.1.0"

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = "#{spec.homepage}/-/blob/main/CHANGELOG.md"
  spec.metadata["rubygems_mfa_required"] = "true"
end
