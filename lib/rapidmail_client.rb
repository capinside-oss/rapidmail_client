# lib/rapidmail_client.rb

require "rapidmail_client/version"
require "rapidmail_client/blacklist_service"
require "rapidmail_client/client"

module RapidmailClient
  class Error < StandardError; end
end
