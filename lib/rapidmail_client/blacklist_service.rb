# lib/rapidmail_client/blacklist_service.rb

module RapidmailClient
  class BlacklistService
    def initialize(client)
      @client = client
    end

    # Get the blacklist items with pagination support
    def list(page: nil, per_page: nil)
      params = {}
      params[:page] = page if page
      params[:per_page] = per_page if per_page

      response = @client.get("blacklist", params)
      {
        blacklist: response.dig("_embedded", "blacklist") || [],
        links: response["_links"],
        page: response["page"],
        page_count: response["page_count"],
        total_items: response["total_items"]
      }
    end

    # Fetch all blacklisted emails across all pages
    def list_all
      all_blacklisted_emails = []
      page = 1

      loop do
        response = list(page: page)
        all_blacklisted_emails.concat(response[:blacklist])
        break if page >= response[:page_count]

        page += 1
      end

      all_blacklisted_emails
    end

    def show(blacklist_id)
      @client.get("blacklist/#{blacklist_id}")
    end

    # Add an email to the blacklist
    def add_to_blacklist(email)
      payload = { pattern: email }
      @client.post("blacklist", payload)
    end

    # Remove email from the blacklist
    def remove_from_blacklist(blacklist_id)
      @client.delete("blacklist/#{blacklist_id}")
    end
  end
end
