# lib/rapidmail_client/rapidmail_client.rb

require "net/http"
require "json"
require "uri"

module RapidmailClient
  class Client
    API_BASE_URL = "https://apiv3.emailsys.net".freeze

    def initialize(username, password)
      @username = username
      @password = password
    end

    # GET request method
    def get(endpoint, params = {})
      uri = URI("#{API_BASE_URL}/#{endpoint}")
      uri.query = URI.encode_www_form(params) unless params.empty?
      request = Net::HTTP::Get.new(uri)

      response = make_request(uri, request)
      parse_response(response)
    end

    # POST request method
    def post(endpoint, payload)
      uri = URI("#{API_BASE_URL}/#{endpoint}")
      request = Net::HTTP::Post.new(uri)
      request["Content-Type"] = "application/json"
      request.body = payload.to_json

      response = make_request(uri, request)
      parse_response(response)
    end

    # DELETE request method
    def delete(endpoint)
      uri = URI("#{API_BASE_URL}/#{endpoint}")
      request = Net::HTTP::Delete.new(uri)

      response = make_request(uri, request)
      parse_response(response)
    end

    private

    def make_request(uri, request)
      request.basic_auth(@username, @password)
      Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == "https") do |http|
        http.request(request)
      end
    end

    def parse_response(response)
      case response
      when Net::HTTPSuccess
        response.body.nil? ? "" : JSON.parse(response.body)
      else
        raise "HTTP Error: #{response.code} - #{response.message}"
      end
    end
  end
end
