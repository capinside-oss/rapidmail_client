# RapidmailClient

Ruby client for Rapidmail API.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rapidmail_client'
```

And then execute:

```bash
$ bundle install
```

Or install it yourself as:

```bash
$ gem install rapidmail_client
```

## Usage

```ruby
require 'rapidmail_client'

# Initialize the client with your username and password
client = RapidmailClient::Client.new('your_username', 'your_password')

# Initialize the blacklist service with the client
blacklist_service = RapidmailClient::BlacklistService.new(client)

# Get the first page of the blacklist
blacklist_page = blacklist_service.get_blacklist(page: 1, per_page: 20)
puts blacklist_page[:show]

# Fetch all blacklisted emails across all pages
all_blacklisted_emails = blacklist_service.get_all_blacklisted_emails
puts all_blacklisted_emails

# Add emails to the blacklist
emails_to_blacklist = ['example1@example.com', 'example2@example.com']
blacklist_service.add_to_blacklist(emails_to_blacklist)

# Remove an email from the blacklist
email_id = 'email_id_to_remove'
blacklist_service.remove_from_blacklist(email_id)
```

## Development
After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to rubygems.org.

## Contributing
Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/capinside-oss/rapidmail_client.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT). rapidmail is a registered trademark of rapidmail GmbH, Freiburg im Breisgau, Deutschland

## Code of Conduct

Everyone interacting in the RapidmailClient project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/capinside-oss/rapidmail_client/-/blob/main/CODE_OF_CONDUCT.md).
